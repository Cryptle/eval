## Simple Math Expression API
### Overview
This API provides a way to solve simple math problems in English. It can validate and evaluate an expression passed in a specific format.

### Features
- Validates a given expression.
- Evaluates a valid expression.
- Tracks the number of invalid requests made to the API.

### API Endpoints
- #### POST /validate 
Accepts a JSON object: 
```json 
  { 
    "expression" : "<a simple math problem>"
  }
```
Returns a JSON object if the expression valid
```json
  {
    "valid" : true
  }
``` 
Returns a JSON object if the expression is not valid
```json
  {
    "valid" : false, 
    "reason" : "<the reason why the expression is invalid>"
  }
``` 
 
- #### POST /evaluate
Accepts a JSON object: 
```json
  {
    "expression" : "<a simple math problem>"
  }
```
Returns a JSON object 
```json 
  {
    "result" : "<the expression's result>"
  }
```
- #### GET /errors
Returns a JSON array of all errors that occurred with their frequency, 
endpoint URL, and error type in the following format:
```json
[
  {
    "expression": "<a simple math problem>",
    "endpoint": "<endpoint URL>",
    "frequency": "<number of times the expression failed>",
    "type": "<error type>"
  }
]
```
## Project Structure
The project is structured into several packages that contains:

**cmd** _main.go_ the entry point to the application.
**pkg/api** the API handlers.
**pkg/mathexpr** the logic for validating and evaluating expressions.
**pkg/errorstore** the ErrorStore structure for managing and storing error occurrences.

### How to Build and Run
To build the project run:
```bash
make build
```

To run the server
```bash
make run
```
this will start the server on port 8080.


## CLI Tool
The CLI tool provides an interface to interact with our application via the command line. It can be used to validate and evaluate expressions, as well as retrieve error records.

### How to use the CLI
First, build the CLI using the provided Makefile. Run the following command:

```bash
make build_cli
```
This will generate an executable file named cli into the cli directory.

#### Evaluate an expression
To evaluate an expression, use the -evaluate flag, followed by the /evaluate endpoint URL, and the -expression flag, followed by the expression you want to evaluate.

```bash
./cli -evaluate=http://localhost:8080/evaluate -expression="What is 3 plus 6 plus 9?"
```

#### Validate an expression
To validate an expression, use the -validate flag, followed by the /validate endpoint URL, and the -expression flag, followed by the expression you want to validate.

```bash
./cli -validate=http://localhost:8080/validate -expression="What is 3 plus 6 plus 9?"
```

#### Retrieve error records
To retrieve error records, use the -errors flag, followed by the /errors endpoint URL.

```bash
./cli -errors=http://localhost:8080/errors
```

## Testing
To run the tests run:
```bash
make tests
```

## Docker
To build the docker image run:
```bash
make docker_build
```
This will build the docker image and run all the tests inside the container.

To run the image run:
```bash
make docker_run
```
this will start the server on port 8080.
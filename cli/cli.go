package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
)

type Expression struct {
	Expression string `json:"expression"`
}

func main() {
	evaluateURL := flag.String("evaluate", "", "URL for the /evaluate endpoint")
	validateURL := flag.String("validate", "", "URL for the /validate endpoint")
	errorsURL := flag.String("errors", "", "URL for the /errors endpoint")
	expression := flag.String("expression", "", "Expression to evaluate or validate")

	flag.Parse()

	// Create expression
	exp := Expression{
		Expression: *expression,
	}
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(exp)

	// Send expression to /evaluate or /validate
	if *evaluateURL != "" {
		res, _ := http.Post(*evaluateURL, "application/json; charset=utf-8", b)
		body, _ := io.ReadAll(res.Body)
		fmt.Println(string(body))
		res.Body.Close()
	} else if *validateURL != "" {
		res, _ := http.Post(*validateURL, "application/json; charset=utf-8", b)
		body, _ := io.ReadAll(res.Body)
		fmt.Println(string(body))
		res.Body.Close()
	} else if *errorsURL != "" {
		res, _ := http.Get(*errorsURL)
		body, _ := io.ReadAll(res.Body)
		fmt.Println(string(body))
		res.Body.Close()
	} else {
		fmt.Println("Please provide an endpoint URL (-evaluate, -validate, or -errors)")
		os.Exit(1)
	}
}

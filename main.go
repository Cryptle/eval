package main

import (
	"gitlab.com/Cryptle/eval/pkg/api"
	"log"
)

func main() {
	srv := api.NewServer()
	if err := srv.Run(":8080"); err != nil {
		log.Fatalf("failed to run server: %v", err)
	}
}

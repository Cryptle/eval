package errorstore

import (
	"fmt"
	"strings"
	"sync"
)

type ErrorEntry struct {
	Expression string `json:"expression"`
	Endpoint   string `json:"endpoint"`
	Frequency  int    `json:"frequency"`
	Type       string `json:"type"`
}

type ErrorStore struct {
	store *sync.Map
}

// New returns a new ErrorStore
func New() *ErrorStore {
	return &ErrorStore{
		store: &sync.Map{},
	}
}

// Increment increments the error count for a given expression and endpoint
func (es *ErrorStore) Increment(expr string, endpoint string) {
	key := fmt.Sprintf("%s:%s", expr, endpoint)
	val, loaded := es.store.LoadOrStore(key, 1)
	if loaded {
		// if the key already exists increment by 1
		newVal := val.(int) + 1
		es.store.Store(key, newVal)
	}
}

// GetAll returns all error entries
func (es *ErrorStore) GetAll() []*ErrorEntry {
	var entries []*ErrorEntry
	es.store.Range(func(key, value any) bool {
		parts := strings.Split(key.(string), ":")
		entries = append(entries, &ErrorEntry{
			Expression: parts[0],
			Endpoint:   parts[1],
			Frequency:  value.(int),
			Type:       "Error",
		})
		return true
	})
	return entries
}

package errorstore

import (
	"testing"
)

func TestErrorStore(t *testing.T) {
	es := New()

	expression := "What is 52 cubed?"
	endpoint := "/evaluate"

	// Initially, there should be no error records
	records := es.GetAll()
	if len(records) != 0 {
		t.Errorf("len(ErrorStore.GetAll()) = %v; want 0", len(records))
	}

	// After incrementing, there should be a record for the expression
	es.Increment(expression, endpoint)
	records = es.GetAll()
	if len(records) != 1 {
		t.Fatalf("len(ErrorStore.GetAll()) = %v; want 1", len(records))
	}
	record := records[0]
	if record.Expression != expression {
		t.Errorf("record.Expression = %v; want %v", record.Expression, expression)
	}
	if record.Endpoint != endpoint {
		t.Errorf("record.Endpoint = %v; want %v", record.Endpoint, endpoint)
	}
	if record.Frequency != 1 {
		t.Errorf("record.Frequency = %v; want 1", record.Frequency)
	}

	// After incrementing again, the count should be 2
	es.Increment(expression, endpoint)
	records = es.GetAll()
	record = records[0]
	if record.Frequency != 2 {
		t.Errorf("record.Frequency = %v; want 2", record.Frequency)
	}
}

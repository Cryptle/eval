build:
	go build -o eval
build_cli:
	go build -o cli ./cli/cli.go
run:
	./eval
tests:
	go test -v ./...
docker_build:
	docker build \
		-t eval:0.0.1 \
		-f docker/Dockerfile .
	docker tag eval:0.0.1 eval/latest
docker_run:
	docker-compose up
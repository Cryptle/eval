package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Cryptle/eval/internal/errorstore"
)

type Server struct {
	router     *gin.Engine
	errorStore *errorstore.ErrorStore
}

// NewServer creates a new server
func NewServer() *Server {
	router := gin.Default()

	server := &Server{
		router:     router,
		errorStore: errorstore.New(),
	}

	router.POST("/validate", server.ValidateHandler)
	router.POST("/evaluate", server.EvaluateHandler)
	router.GET("/errors", server.ErrorsHandler)

	return server
}

// Run starts the server on the given address
func (s *Server) Run(addr string) error {
	return s.router.Run(addr)
}

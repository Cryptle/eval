package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Cryptle/eval/pkg/mathexpr"
	"net/http"
)

type Expression struct {
	Expression string `json:"expression"`
}

type ValidationResult struct {
	Valid  bool   `json:"valid"`
	Reason string `json:"reason,omitempty"`
}

type EvaluationResult struct {
	Result int `json:"result"`
}

// ValidateHandler validates an expression
func (s *Server) ValidateHandler(c *gin.Context) {
	var expr Expression
	if err := c.BindJSON(&expr); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	validator := mathexpr.NewValidator()
	valid, reason := validator.ValidateExpression(expr.Expression)
	if !valid {
		s.errorStore.Increment(expr.Expression, c.FullPath())
		c.JSON(http.StatusOK, ValidationResult{Valid: false, Reason: reason})
		return
	}
	c.JSON(http.StatusOK, ValidationResult{Valid: true})
}

// EvaluateHandler evaluates an expression
func (s *Server) EvaluateHandler(c *gin.Context) {
	var expr Expression
	if err := c.BindJSON(&expr); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	evaluator := mathexpr.NewEvaluator()
	result, err := evaluator.EvaluateExpression(expr.Expression)
	if err != nil {
		s.errorStore.Increment(expr.Expression, c.FullPath())
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, EvaluationResult{Result: result})
}

// ErrorsHandler returns all errors
func (s *Server) ErrorsHandler(c *gin.Context) {
	c.JSON(http.StatusOK, s.errorStore.GetAll())
}

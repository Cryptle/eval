package api

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestValidateHandler(t *testing.T) {
	gin.SetMode(gin.TestMode)

	testCases := []struct {
		name       string
		expression string
		wantValid  bool
	}{
		{name: "Valid expression", expression: "What is 5 plus 13?", wantValid: true},
		{name: "Invalid expression", expression: "What is 52 cubed?", wantValid: false},
		{name: "Valid expression", expression: "What is 6 multiplied by 4?", wantValid: true},
		{name: "Valid expression", expression: "What is 25 divided by 5?", wantValid: true},
		{name: "Valid expression", expression: "What is 3 plus 2 multiplied by 3?", wantValid: true},
		{name: "Invalid expression", expression: "Who is the President of Bulgaria?", wantValid: false},
		{name: "Invalid expression", expression: "What is 1 plus plus 2?", wantValid: false},
		{name: "Invalid expression", expression: "", wantValid: false},
	}

	server := NewServer()
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			router := gin.Default()
			router.POST("/validate", server.ValidateHandler)
			bodyData := map[string]string{
				"expression": tc.expression,
			}
			body, err := json.Marshal(bodyData)
			assert.NoError(t, err)

			req, err := http.NewRequest(http.MethodPost, "/validate", bytes.NewBuffer(body))
			assert.NoError(t, err)

			req.Header.Set("Content-Type", "application/json")
			resp := httptest.NewRecorder()

			router.ServeHTTP(resp, req)
			assert.Equal(t, http.StatusOK, resp.Code)

			var result ValidationResult
			if err = json.Unmarshal(resp.Body.Bytes(), &result); err != nil {
				t.Fatalf("cannot parse json response: %v", err)
			}

			assert.Equal(t, tc.wantValid, result.Valid)
		})
	}
}

func TestEvaluateHandler(t *testing.T) {
	gin.SetMode(gin.TestMode)

	testCases := []struct {
		name       string
		expression string
		wantResult int
		hasError   bool
	}{
		{
			name:       "Addition",
			expression: "What is 5 plus 13?",
			wantResult: 18,
		},
		{
			name:       "Non-mathematical question",
			expression: "What is 5 ?",
			wantResult: 5,
		},
		{
			name:       "Subtraction",
			expression: "What is 7 minus 5?",
			wantResult: 2,
		},
		{
			name:       "Multiplication",
			expression: "What is 6 multiplied by 4?",
			wantResult: 24,
		},
		{
			name:       "Division",
			expression: "What is 25 divided by 5?",
			wantResult: 5,
		},
		{
			name:       "Multiple operations",
			expression: "What is 3 plus 2 multiplied by 3?",
			wantResult: 15,
		},
	}

	server := NewServer()

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			router := gin.Default()
			router.POST("/evaluate", server.EvaluateHandler)

			bodyData := map[string]string{
				"expression": tc.expression,
			}
			body, _ := json.Marshal(bodyData)

			req, _ := http.NewRequest(http.MethodPost, "/evaluate", bytes.NewBuffer(body))
			req.Header.Set("Content-Type", "application/json")
			resp := httptest.NewRecorder()

			router.ServeHTTP(resp, req)

			if tc.hasError {
				assert.NotEqual(t, http.StatusOK, resp.Code)
			} else {
				assert.Equal(t, http.StatusOK, resp.Code)

				var result EvaluationResult
				err := json.Unmarshal(resp.Body.Bytes(), &result)
				if err != nil {
					t.Fatalf("Cannot parse json response: %v", err)
				}

				assert.Equal(t, tc.wantResult, result.Result)
			}
		})
	}
}

func TestErrorsHandler(t *testing.T) {
	server := NewServer()
	ts := httptest.NewServer(server.router)
	defer ts.Close()

	client := &http.Client{}

	preReq, err := http.NewRequest("POST", ts.URL+"/validate", strings.NewReader(`{"expression": "2+2"}`))
	assert.NoError(t, err)
	preReq.Header.Set("Content-Type", "application/json")
	_, err = client.Do(preReq)
	assert.NoError(t, err)

	preReq, err = http.NewRequest("POST", ts.URL+"/validate", strings.NewReader(`{"expression": "3-1"}`))
	assert.NoError(t, err)
	preReq.Header.Set("Content-Type", "application/json")
	_, err = client.Do(preReq)
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", ts.URL+"/errors", nil)
	assert.NoError(t, err)

	resp, err := client.Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	defer resp.Body.Close()

	bodyBytes, err := io.ReadAll(resp.Body)
	assert.NoError(t, err)
	bodyString := string(bodyBytes)

	expected := `[{"expression":"2+2","endpoint":"/validate","frequency":1,"type":"Error"},{"expression":"3-1","endpoint":"/validate","frequency":1,"type":"Error"}]`
	if bodyString != expected {
		t.Errorf("Expected %s\nActual   %s", expected, bodyString)
	}
}

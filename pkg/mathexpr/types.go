package mathexpr

type Operator string

func (o Operator) String() string {
	return string(o)
}

// Operators
const (
	Plus       Operator = "plus"
	Minus      Operator = "minus"
	Multiply   Operator = "multiply"
	Multiplied Operator = "multiplied"
	Divide     Operator = "divide"
	Divided    Operator = "divided"
	By         Operator = "by"
	Cubed      Operator = "cubed"
)

// Validator interface for validating expressions
type Validator interface {
	ValidateExpression(exp string) (bool, string)
}

// Evaluator interface for evaluating expressions
type Evaluator interface {
	EvaluateExpression(exp string) (int, error)
}

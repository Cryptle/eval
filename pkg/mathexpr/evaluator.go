package mathexpr

import (
	"strconv"
	"strings"
)

// NewEvaluator returns a new evaluator
func NewEvaluator() Evaluator {
	return &evaluator{}
}

type evaluator struct {
}

// EvaluateExpression evaluates a math expression
func (e *evaluator) EvaluateExpression(exp string) (int, error) {
	// split the expression into words and initialize result and operator
	var (
		words             = strings.Split(exp, " ")
		result            = 0
		operator Operator = ""
	)

	// loop through words
	for _, word := range words {
		switch word {
		case Plus.String():
			operator = Plus
		case Minus.String():
			operator = Minus
		case Multiplied.String():
			operator = Multiply
		case Divided.String():
			operator = Divide
		default:
			// if the word ends with a '?' remove it
			if word[len(word)-1] == '?' {
				word = word[:len(word)-1]
			}
			num, err := strconv.Atoi(word)
			if err != nil {
				continue
			}
			// apply operator
			switch operator {
			case Plus:
				result += num
			case Minus:
				result -= num
			case Multiply:
				result *= num
			case Divide:
				result /= num
			default:
				result = num
			}
		}
	}

	return result, nil
}

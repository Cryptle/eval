package mathexpr

import "testing"

func TestValidateExpression(t *testing.T) {
	testCases := []struct {
		expr     string
		expected bool
		reason   string
	}{
		{"What is 5 plus 13 ?", true, ""},
		{"What is 7 minus 5?", true, ""},
		{"What is 6 multiplied by 4?", true, ""},
		{"What is 25 divided by 5?", true, ""},
		{"What is 3 plus 2 multiplied by 3?", true, ""},
		{"What is 52 cubed?", false, "Unsupported operation: cubed"},
		{"Who is the President of the United States?", false, "Non-mathematical question"},
		{"What is 1 plus plus 2?", false, "Invalid syntax"},
		{"", false, "Empty expression"},
	}
	val := NewValidator()
	for _, tc := range testCases {
		result, reason := val.ValidateExpression(tc.expr)
		if result != tc.expected || reason != tc.reason {
			t.Errorf("validateExpression(%s) = %v, %v; want %v, %v", tc.expr, result, reason, tc.expected, tc.reason)
		}
	}
}

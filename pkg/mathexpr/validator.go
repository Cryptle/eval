package mathexpr

import (
	"strconv"
	"strings"
)

// NewValidator returns a new validator
func NewValidator() Validator {
	return &validator{}
}

type validator struct{}

// ValidateExpression validates a math expression
func (v *validator) ValidateExpression(exp string) (bool, string) {
	var (
		words          = strings.Fields(strings.TrimSuffix(exp, "?"))
		validOperators = map[Operator]bool{
			Plus:       true,
			Minus:      true,
			Multiplied: true,
			Divided:    true,
			By:         true,
		}
		unsupportedOperations = map[Operator]bool{
			Cubed: true,
		}
		prevWordIsOperator bool
	)

	// if the expression is empty, return false with a reason
	if len(words) < 3 {
		return false, "Empty expression"
	}

	// if the first two words are not "What is", return false with a reason
	if words[0] != "What" || words[1] != "is" {
		return false, "Non-mathematical question"
	}

	// validate the first number
	if _, err := strconv.Atoi(words[2]); err != nil {
		return false, "Invalid syntax: " + words[2]
	}

	// go through the rest of the words and check the operators and numbers
	for i := 3; i < len(words); i++ {
		word := words[i]

		if word == By.String() && i > 3 && (words[i-1] == Multiplied.String() || words[i-1] == Divided.String()) {
			continue
		}

		// Check if the word is a valid operator
		if _, ok := validOperators[Operator(word)]; ok {
			if prevWordIsOperator {
				return false, "Invalid syntax"
			}
			prevWordIsOperator = true
			continue
		}

		// check if the word is an unsupported operation
		if _, ok := unsupportedOperations[Operator(word)]; ok {
			return false, "Unsupported operation: " + word
		}

		// check if the word is a valid number
		if _, err := strconv.Atoi(word); err != nil {
			return false, "Invalid syntax: " + word
		}

		prevWordIsOperator = false
	}

	return true, ""
}

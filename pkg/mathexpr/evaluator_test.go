package mathexpr

import "testing"

func TestEvaluateExpression(t *testing.T) {
	tests := []struct {
		expression string
		want       int
	}{
		{"What is 5 ?", 5},
		{"What is 5 plus 13 ?", 18},
		{"What is 7 minus 5?", 2},
		{"What is 6 multiplied by 4?", 24},
		{"What is 25 divided by 5?", 5},
		{"What is 3 plus 2 multiplied by 3?", 15},
	}
	eval := NewEvaluator()
	for _, test := range tests {
		got, err := eval.EvaluateExpression(test.expression)
		if err != nil {
			t.Errorf("evaluateExpression(%q) returned error: %v", test.expression, err)
		}
		if got != test.want {
			t.Errorf("evaluateExpression(%q) = %v, want %v", test.expression, got, test.want)
		}
	}
}
